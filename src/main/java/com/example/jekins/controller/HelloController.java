package com.example.jekins.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ming
 * @date 2021/12/24
 */
@RestController
@RequestMapping("hello")
public class HelloController {
    @GetMapping
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("index")
    public String index(){
        return "index";
    }
}
